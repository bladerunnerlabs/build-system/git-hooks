# git-hooks

Git client-side hook scripts.

## pre-commit

Invokes `black` and `pylint` (relevant for Python).

Should be placed in `.git/hooks/pre-commit`.

Note that the hook does not contain the specific arguments for these tools.
Instead these arguments should be configured per-project in `pyproject.toml`.
See `pyproject.toml-template`.



